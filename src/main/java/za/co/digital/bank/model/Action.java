package za.co.digital.bank.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import za.co.digital.bank.enums.Status;
import za.co.digital.bank.enums.ValueType;

@Document
public class Action {
    @Transient
    public static final String SEQUENCE_NAME = "action_sequence";

    @Id
    private long actionId;
    private long commandId;
    private String name;
    private ValueType valueType;// (0:class/1:string-value/2:decimal-value/3:int-value/4:date-value) (int)
    private String targetClassName;
    private String targetClassFieldName;
    private String sourceClassName;
    private String sourceClassFieldName;
    private String sourceValue;
    private Status status = Status.ACTIVE;

    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    public long getCommandId() {
        return commandId;
    }

    public void setCommandId(long commandId) {
        this.commandId = commandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public String getTargetClassName() {
        return targetClassName;
    }

    public void setTargetClassName(String targetClassName) {
        this.targetClassName = targetClassName;
    }

    public String getTargetClassFieldName() {
        return targetClassFieldName;
    }

    public void setTargetClassFieldName(String targetClassFieldName) {
        this.targetClassFieldName = targetClassFieldName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceClassFieldName() {
        return sourceClassFieldName;
    }

    public void setSourceClassFieldName(String sourceClassFieldName) {
        this.sourceClassFieldName = sourceClassFieldName;
    }

    public String getSourceValue() {
        return sourceValue;
    }

    public void setSourceValue(String sourceValue) {
        this.sourceValue = sourceValue;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}