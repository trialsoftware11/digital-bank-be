package za.co.digital.bank.model;

public enum UserRole {
    SUPER_USER("SU"),
    VENDOR_ADMIN("VA"),
    DEPT_ADMIN("DA"),
    DEPT_USER("DU"),
    CLIENT_USER("CU");

    private String code;

    UserRole(String code){
        this.code=code;
    }
}