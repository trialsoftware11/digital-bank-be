package za.co.digital.bank.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import za.co.digital.bank.enums.InstructionType;
import za.co.digital.bank.enums.Status;

import java.util.Date;

@Document
public class Instruction {

    @Transient
    public static final String SEQUENCE_NAME = "instruction_sequence";

    @Id
    private long instructionId;

    private String name;
    private String notes;
    private InstructionType instructionType;
    private long initiatingUserId;
    private long executingUserId;
    private long approvalUserId;
    private int approvalStatus;
    private int executionStatus;
    private Date instructionCreationDate;
    private Date instructionExecutionDate;
    private Status status = Status.ACTIVE;

    public long getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(long instructionId) {
        this.instructionId = instructionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public InstructionType getInstructionType() {
        return instructionType;
    }

    public void setInstructionType(InstructionType instructionType) {
        this.instructionType = instructionType;
    }

    public long getInitiatingUserId() {
        return initiatingUserId;
    }

    public void setInitiatingUserId(long initiatingUserId) {
        this.initiatingUserId = initiatingUserId;
    }

    public long getExecutingUserId() {
        return executingUserId;
    }

    public void setExecutingUserId(long executingUserId) {
        this.executingUserId = executingUserId;
    }

    public long getApprovalUserId() {
        return approvalUserId;
    }

    public void setApprovalUserId(long approvalUserId) {
        this.approvalUserId = approvalUserId;
    }

    public int getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(int approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public int getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(int executionStatus) {
        this.executionStatus = executionStatus;
    }

    public Date getInstructionCreationDate() {
        return instructionCreationDate;
    }

    public void setInstructionCreationDate(Date instructionCreationDate) {
        this.instructionCreationDate = instructionCreationDate;
    }

    public Date getInstructionExecutionDate() {
        return instructionExecutionDate;
    }

    public void setInstructionExecutionDate(Date instructionExecutionDate) {
        this.instructionExecutionDate = instructionExecutionDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}