package za.co.digital.bank.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import za.co.digital.bank.enums.VendorStatus;

import java.util.List;

@Document
public class Vendor {

    @Transient
    public static final String SEQUENCE_NAME = "vendors_sequence";

    @Id
    private long id;
    private String name;
    private VendorStatus status;

    @Transient
    private List<Department> departments;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VendorStatus getStatus() {
        return status;
    }

    public void setStatus(VendorStatus status) {
        this.status = status;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
}