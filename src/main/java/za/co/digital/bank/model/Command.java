package za.co.digital.bank.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import za.co.digital.bank.enums.CommandSource;
import za.co.digital.bank.enums.Status;

import java.util.Date;

@Document
public class Command {
    @Transient
    public static final String SEQUENCE_NAME = "command_sequence";

    @Id
    private long commandId;

    private long instructionId;
    private String name;
    private CommandSource commandSource;
    private Date commandCreationDate;
    private long userInitiating;
    private int accountId;
    private int assetId;
    private Status status = Status.ACTIVE;

    public long getCommandId() {
        return commandId;
    }

    public void setCommandId(long commandId) {
        this.commandId = commandId;
    }

    public long getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(long instructionId) {
        this.instructionId = instructionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommandSource getCommandSource() {
        return commandSource;
    }

    public void setCommandSource(CommandSource commandSource) {
        this.commandSource = commandSource;
    }

    public Date getCommandCreationDate() {
        return commandCreationDate;
    }

    public void setCommandCreationDate(Date commandCreationDate) {
        this.commandCreationDate = commandCreationDate;
    }

    public long getUserInitiating() {
        return userInitiating;
    }

    public void setUserInitiating(long userInitiating) {
        this.userInitiating = userInitiating;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}