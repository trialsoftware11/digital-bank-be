package za.co.digital.bank.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import za.co.digital.bank.enums.JoinType;
import za.co.digital.bank.enums.Status;
import za.co.digital.bank.enums.ValueType;

@Document
public class Parameter {

    @Transient
    public static final String SEQUENCE_NAME = "action_parameter_sequence";

    @Id
    private long parameterId;
    private long actionId;
    private String parameterName;
    private String parameterFieldName;
    private String parameterValue;
    private ValueType parameterValueType;
    private String filterCondition;
    private JoinType nextJoinType;
    private Status status = Status.ACTIVE;

    public long getParameterId() {
        return parameterId;
    }

    public void setParameterId(long parameterId) {
        this.parameterId = parameterId;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterFieldName() {
        return parameterFieldName;
    }

    public void setParameterFieldName(String parameterFieldName) {
        this.parameterFieldName = parameterFieldName;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public ValueType getParameterValueType() {
        return parameterValueType;
    }

    public void setParameterValueType(ValueType parameterValueType) {
        this.parameterValueType = parameterValueType;
    }

    public String getFilterCondition() {
        return filterCondition;
    }

    public void setFilterCondition(String filterCondition) {
        this.filterCondition = filterCondition;
    }

    public JoinType getNextJoinType() {
        return nextJoinType;
    }

    public void setNextJoinType(JoinType nextJoinType) {
        this.nextJoinType = nextJoinType;
    }

    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}