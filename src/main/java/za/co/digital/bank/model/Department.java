package za.co.digital.bank.model;

import org.springframework.data.annotation.Transient;

public class Department extends Vendor {

    @Transient
    public static final String SEQUENCE_NAME = "departments_sequence";

    private long vendorId;

    public long getVendorId() {
        return vendorId;
    }

    public void setVendorId(long vendorId) {
        this.vendorId = vendorId;
    }
}