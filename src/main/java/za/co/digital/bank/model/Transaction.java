package za.co.digital.bank.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import za.co.digital.bank.enums.ValueType;

@Document
public class Transaction {
    @Transient
    public static final String SEQUENCE_NAME = "transaction_sequence";

    @Id
    private long transactionId;
    private String name;
    private ValueType transactionValueType;
    private String targetClassName;
    private String targetClassFieldName;
    private String sourceClassName;
    private String sourceClassFieldName;
    private String sourceValue;

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ValueType getTransactionValueType() {
        return transactionValueType;
    }

    public void setTransactionValueType(ValueType transactionValueType) {
        this.transactionValueType = transactionValueType;
    }

    public String getTargetClassName() {
        return targetClassName;
    }

    public void setTargetClassName(String targetClassName) {
        this.targetClassName = targetClassName;
    }

    public String getTargetClassFieldName() {
        return targetClassFieldName;
    }

    public void setTargetClassFieldName(String targetClassFieldName) {
        this.targetClassFieldName = targetClassFieldName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceClassFieldName() {
        return sourceClassFieldName;
    }

    public void setSourceClassFieldName(String sourceClassFieldName) {
        this.sourceClassFieldName = sourceClassFieldName;
    }

    public String getSourceValue() {
        return sourceValue;
    }

    public void setSourceValue(String sourceValue) {
        this.sourceValue = sourceValue;
    }
}