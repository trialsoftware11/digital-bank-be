package za.co.digital.bank.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;
import za.co.digital.bank.enums.Status;

@Document
public class Customer {
    @Transient
    public static final String SEQUENCE_NAME = "customer_sequence";

    @Id
    private long customerId;

    private int clientUserId;
    private String firstName;
    private String lastName;
    private boolean male;
    private boolean female;
    private boolean married;
    private boolean disability;
    private boolean selfEmployed;
    private boolean lifeInsured;
    private boolean medicalAid;
    private boolean pension;
    private double salaryOnDebitOrder;
    private Date dateOfBirth;
    private int age;
    private double positiveBalanceScore;
    private double increasingBalanceScore;
    private Status status;

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public int getClientUserId() {
        return clientUserId;
    }

    public void setClientUserId(int clientUserId) {
        this.clientUserId = clientUserId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public boolean isFemale() {
        return female;
    }

    public void setFemale(boolean female) {
        this.female = female;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    public boolean isDisability() {
        return disability;
    }

    public void setDisability(boolean disability) {
        this.disability = disability;
    }

    public boolean isSelfEmployed() {
        return selfEmployed;
    }

    public void setSelfEmployed(boolean selfEmployed) {
        this.selfEmployed = selfEmployed;
    }

    public boolean isLifeInsured() {
        return lifeInsured;
    }

    public void setLifeInsured(boolean lifeInsured) {
        this.lifeInsured = lifeInsured;
    }

    public boolean isMedicalAid() {
        return medicalAid;
    }

    public void setMedicalAid(boolean medicalAid) {
        this.medicalAid = medicalAid;
    }

    public boolean isPension() {
        return pension;
    }

    public void setPension(boolean pension) {
        this.pension = pension;
    }

    public double getSalaryOnDebitOrder() {
        return salaryOnDebitOrder;
    }

    public void setSalaryOnDebitOrder(double salaryOnDebitOrder) {
        this.salaryOnDebitOrder = salaryOnDebitOrder;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getPositiveBalanceScore() {
        return positiveBalanceScore;
    }

    public void setPositiveBalanceScore(double positiveBalanceScore) {
        this.positiveBalanceScore = positiveBalanceScore;
    }

    public double getIncreasingBalanceScore() {
        return increasingBalanceScore;
    }

    public void setIncreasingBalanceScore(double increasingBalanceScore) {
        this.increasingBalanceScore = increasingBalanceScore;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}