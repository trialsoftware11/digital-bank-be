package za.co.digital.bank.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import za.co.digital.bank.enums.Status;
import za.co.digital.bank.enums.AccountType;

@Document
public class Account {
    @Transient
    public static final String SEQUENCE_NAME = "account_sequence";

    @Id
    private long accountId;

    private AccountType accountType;
    private long ownerId;
    private double minimumBalance;
    private double balance;
    private double lastBalance;
    private int score;
    private int withdrawalRestriction;
    private double withdrawalLimit;
    private double overdrawLimit;
    private double accrualRate;
    private Status status = Status.ACTIVE;

    @Transient
    private Customer owner;

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public double getMinimumBalance() {
        return minimumBalance;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public void setMinimumBalance(double minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getLastBalance() {
        return lastBalance;
    }

    public void setLastBalance(double lastBalance) {
        this.lastBalance = lastBalance;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getWithdrawalRestriction() {
        return withdrawalRestriction;
    }

    public void setWithdrawalRestriction(int withdrawalRestriction) {
        this.withdrawalRestriction = withdrawalRestriction;
    }

    public double getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setWithdrawalLimit(double withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }

    public double getOverdrawLimit() {
        return overdrawLimit;
    }

    public void setOverdrawLimit(double overdrawLimit) {
        this.overdrawLimit = overdrawLimit;
    }

    public double getAccrualRate() {
        return accrualRate;
    }

    public void setAccrualRate(double accrualRate) {
        this.accrualRate = accrualRate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }
}