package za.co.digital.bank.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Dummy {

    @Transient
    public static final String SEQUENCE_NAME = "dummy_sequence";

    ObjectId _id = new ObjectId("507f1f77bcf86cd799439011");


    private double savings = 1400;
    private double current = 3625.20;
    private double totalCash = 0;
    private double callAccount = 78565;
    private double unitTrust = 14552;
    private double totalInvestments = 0;
    private double vehicleLoan = 524444;
    private double personalLoan = 150700;
    private double totalLoans = 0;
    private double residentialLoan = 1405000;
    private double totalMortgages = 0;

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public double getSavings() {
        return savings;
    }

    public void setSavings(double savings) {
        this.savings = savings;
    }

    public double getCurrent() {
        return current;
    }

    public void setCurrent(double current) {
        this.current = current;
    }

    public double getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(double totalCash) {
        this.totalCash = totalCash;
    }

    public double getCallAccount() {
        return callAccount;
    }

    public void setCallAccount(double callAccount) {
        this.callAccount = callAccount;
    }

    public double getUnitTrust() {
        return unitTrust;
    }

    public void setUnitTrust(double unitTrust) {
        this.unitTrust = unitTrust;
    }

    public double getTotalInvestments() {
        return totalInvestments;
    }

    public void setTotalInvestments(double totalInvestments) {
        this.totalInvestments = totalInvestments;
    }

    public double getVehicleLoan() {
        return vehicleLoan;
    }

    public void setVehicleLoan(double vehicleLoan) {
        this.vehicleLoan = vehicleLoan;
    }

    public double getPersonalLoan() {
        return personalLoan;
    }

    public void setPersonalLoan(double personalLoan) {
        this.personalLoan = personalLoan;
    }

    public double getTotalLoans() {
        return totalLoans;
    }

    public void setTotalLoans(double totalLoans) {
        this.totalLoans = totalLoans;
    }

    public double getResidentialLoan() {
        return residentialLoan;
    }

    public void setResidentialLoan(double residentialLoan) {
        this.residentialLoan = residentialLoan;
    }

    public double getTotalMortgages() {
        return totalMortgages;
    }

    public void setTotalMortgages(double totalMortgages) {
        this.totalMortgages = totalMortgages;
    }
}