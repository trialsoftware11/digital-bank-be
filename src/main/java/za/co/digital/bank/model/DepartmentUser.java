package za.co.digital.bank.model;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class DepartmentUser extends  User {
    @Transient
    public static final String SEQUENCE_NAME = "users_sequence";
}