package za.co.digital.bank.model;

import org.springframework.data.annotation.Transient;

public class ClientUser extends  User {
    @Transient
    public static final String SEQUENCE_NAME = "users_sequence";
}