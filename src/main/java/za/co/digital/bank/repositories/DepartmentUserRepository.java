package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import za.co.digital.bank.model.DepartmentUser;
import za.co.digital.bank.model.VendorUser;

import java.util.List;

public interface DepartmentUserRepository extends MongoRepository<DepartmentUser, Long> {
    @Query("{username: '?0', password: '?1'}")
    DepartmentUser authenticate(String username, String password);

    @Query("{username: '?0', email: '?1'}")
    DepartmentUser findByUsernameAndEmail(String username, String email);

    @Query("{forgotPasswordToken: '?0'}")
    DepartmentUser findByToken(String token);

    @Query("{activationToken: '?0'}")
    DepartmentUser findByActivationToken(String activationToken);

    @Query("{departmentId: ?0}")
    List<DepartmentUser> findByDepartmentId(long departmentId);

    @Query("{vendorId: ?0}")
    List<DepartmentUser> findByVendorId(long vendorId);
}