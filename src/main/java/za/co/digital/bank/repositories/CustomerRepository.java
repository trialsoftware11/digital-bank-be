package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Customer;

public interface CustomerRepository extends  MongoRepository<Customer, Long> {
}