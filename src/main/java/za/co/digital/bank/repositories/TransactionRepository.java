package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Transaction;

public interface TransactionRepository extends  MongoRepository<Transaction, Long> {
}