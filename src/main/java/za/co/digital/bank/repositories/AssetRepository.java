package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Asset;

public interface AssetRepository extends  MongoRepository<Asset, Long> {
}