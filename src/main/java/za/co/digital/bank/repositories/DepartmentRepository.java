package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Department;

import java.util.List;

public interface DepartmentRepository extends MongoRepository<Department, Long> {

    List<Department> findByVendorId(Long vendorId);
}