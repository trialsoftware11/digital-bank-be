package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Action;

public interface ActionRepository extends  MongoRepository<Action, Long> {
}