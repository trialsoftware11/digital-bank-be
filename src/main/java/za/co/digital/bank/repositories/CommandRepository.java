package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Command;

public interface CommandRepository extends  MongoRepository<Command, Long> {
}