package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Instruction;

public interface InstructionRepository extends  MongoRepository<Instruction, Long> {
}