package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Parameter;

public interface ParameterRepository extends  MongoRepository<Parameter, Long> {
}