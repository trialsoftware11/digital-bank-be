package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import za.co.digital.bank.model.Account;

import java.util.List;

public interface AccountRepository extends  MongoRepository<Account, Long> {
    @Query("{ 'ownerId' : ?0 }")
    public List<Account> findByOwnerId(long ownerId);
}