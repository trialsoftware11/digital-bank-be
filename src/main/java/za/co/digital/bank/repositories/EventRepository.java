package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Event;

public interface EventRepository extends  MongoRepository<Event, Long> {
}