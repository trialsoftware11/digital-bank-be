package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import za.co.digital.bank.model.ClientUser;

public interface ClientUserRepository extends MongoRepository<ClientUser,Long> {
    @Query("{username: '?0', password: '?1'}")
    ClientUser authenticate(String username, String password);

    @Query("{username: '?0', email: '?1'}")
    ClientUser findByUsernameAndEmail(String username, String email);

    @Query("{forgotPasswordToken: '?0'}")
    ClientUser findByToken(String token);

    @Query("{activationToken: '?0'}")
    ClientUser findByActivationToken(String activationToken);
}