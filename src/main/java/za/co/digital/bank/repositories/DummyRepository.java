package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Dummy;

public interface DummyRepository extends  MongoRepository<Dummy, String> {

}