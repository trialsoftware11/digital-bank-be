package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import za.co.digital.bank.model.UserRole;
import za.co.digital.bank.model.VendorUser;

import java.util.List;

public interface VendorUserRepository extends MongoRepository<VendorUser,Long> {
    @Query("{username: '?0', password: '?1'}")
    VendorUser authenticate(String username, String password);

    @Query("{username: '?0', email: '?1'}")
    VendorUser findByUsernameAndEmail(String username, String email);

    @Query("{forgotPasswordToken: '?0'}")
    VendorUser findByToken(String token);

    @Query("{activationToken: '?0'}")
    VendorUser findByActivationToken(String activationToken);

    @Query("{_id: ?0, role: '?1'}")
    VendorUser findByIdAndRole(long id, String role);

    @Query("{vendorId: ?0}")
    List<VendorUser> findByVendorId(long vendorId);
}