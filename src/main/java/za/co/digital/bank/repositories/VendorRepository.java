package za.co.digital.bank.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.digital.bank.model.Vendor;

public interface VendorRepository extends MongoRepository<Vendor,Long> {
}