package za.co.digital.bank.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Department;
import za.co.digital.bank.services.DepartmentService;

@RestController
@RequestMapping(path="/v1/department")
    public class DepartmentController  extends AbstractController<DepartmentService, Department> {

}