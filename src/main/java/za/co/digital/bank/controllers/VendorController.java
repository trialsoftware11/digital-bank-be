package za.co.digital.bank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.digital.bank.services.VendorService;
import za.co.digital.bank.model.Vendor;
import za.co.digital.bank.utils.Message;

import java.util.List;

@RestController
@RequestMapping(path="/v1/vendor")
public class VendorController {

    @Autowired
    VendorService vendorService;

    @PostMapping(path="/save")
    public ResponseEntity<Vendor> save(@RequestBody Vendor vendor){
        return ResponseEntity.ok(vendorService.save(vendor));
    }

    @GetMapping(path="/getAll")
    public ResponseEntity<List<Vendor>> getAll(){
        return ResponseEntity.ok(vendorService.findAll());
    }

    @GetMapping(path="/findById/{id}")
    public ResponseEntity<Vendor> findById(@PathVariable long id){
        return ResponseEntity.ok(vendorService.findById(id));
    }

    @GetMapping(path="/delete/{id}")
    public ResponseEntity<Message> delete(@PathVariable Long id){
        return ResponseEntity.ok(vendorService.delete(id));
    }
}