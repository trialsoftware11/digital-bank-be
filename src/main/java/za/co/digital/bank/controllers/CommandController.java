package za.co.digital.bank.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Command;
import za.co.digital.bank.services.CommandService;

@RestController
@RequestMapping(path="/v1/command")
public class CommandController extends AbstractController<CommandService, Command> {
}