package za.co.digital.bank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import za.co.digital.bank.services.AbstractDAO;

import java.util.List;

public class AbstractController <T extends AbstractDAO, S> {

    @Autowired
    T service;

    @PostMapping(path="/save")
    public ResponseEntity<S> save(@RequestBody S vendor){
        return ResponseEntity.ok((S) service.save(vendor));
    }

    @GetMapping(path="/findAll")
    public ResponseEntity<List<S>> findAll(){
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping(path="/findById/{id}")
    public ResponseEntity<S> findById(@PathVariable long id){
        return ResponseEntity.ok((S)service.findById(id));
    }

    @GetMapping(path="/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable long id){
        service.delete(id);
        return ResponseEntity.ok(String.format("Object with id %d deleted.", id));
    }
}