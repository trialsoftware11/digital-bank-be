package za.co.digital.bank.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Asset;
import za.co.digital.bank.services.AssetService;

@RestController
@RequestMapping(path="/v1/asset")
public class AssetController extends AbstractController<AssetService, Asset> {
}