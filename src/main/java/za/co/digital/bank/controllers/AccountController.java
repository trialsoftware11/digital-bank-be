package za.co.digital.bank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Customer;
import za.co.digital.bank.services.AccountService;
import za.co.digital.bank.services.CustomerService;
import za.co.digital.bank.model.Account;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/v1/account")
public class AccountController extends AbstractController<AccountService, Account> {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AccountService accountService;

    @GetMapping(path = "/findById/{id}")
    public ResponseEntity<Account> findById(@PathVariable long id) {
        Optional<Account> accountOptional = service.findById(id);
        Account account = accountOptional.get();

        Optional<Customer> customerOptional = customerService.findById(account.getOwnerId());
        if (customerOptional.isPresent()) {
            account.setOwner(customerOptional.get());
        }
        return ResponseEntity.ok(account);
    }

    @GetMapping(path = "/findByOwnerId/{ownerId}")
    public ResponseEntity<List<Account>> findByOwnerId(@PathVariable long ownerId) {
        Optional<Customer> customerOptional = customerService.findById(ownerId);
        List<Account> accounts = accountService.findByOwnerId(ownerId);
        for(Account account: accounts){
            account.setOwner(customerOptional.get());
        }
        return ResponseEntity.ok(accounts);
    }

}