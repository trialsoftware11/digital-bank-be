package za.co.digital.bank.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Parameter;
import za.co.digital.bank.services.ParameterService;

@RestController
@RequestMapping(path="/v1/parameter")
public class ParameterController extends AbstractController<ParameterService, Parameter> {
}