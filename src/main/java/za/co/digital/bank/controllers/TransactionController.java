package za.co.digital.bank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Dummy;
import za.co.digital.bank.model.Transaction;
import za.co.digital.bank.repositories.DummyRepository;
import za.co.digital.bank.services.TransactionService;

@RestController
@RequestMapping(path="/v1/transaction")
public class TransactionController extends AbstractController<TransactionService, Transaction> {

    @Autowired
    DummyRepository dummyRepository;

    @GetMapping(path="/findDummyTransactions")
    public ResponseEntity<Dummy> findDummyTransactions(){
        return ResponseEntity.ok(dummyRepository.findById("507f1f77bcf86cd799439011").get());
    }
}