package za.co.digital.bank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.digital.bank.model.User;
import za.co.digital.bank.utils.Message;
import za.co.digital.bank.utils.Messages;
import za.co.digital.bank.services.UserService;

import java.util.List;

@RestController
@RequestMapping(path="/v1/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(path = "/ping")
    public ResponseEntity<String> ping(){
        return ResponseEntity.ok("Ping.");
    }

    @PostMapping(path="/authenticate", consumes = "application/json")
    public ResponseEntity<Object> authenticate(@RequestBody User user){
        User found = userService.authenticate(user);
        if(found != null) {
            return ResponseEntity.ok(found);
        }else{
            return ResponseEntity.ok(new Message(Messages.INVALID_USER));
        }
    }

    @PostMapping(path="/register")
    public ResponseEntity<User> register(@RequestBody User user){
        return ResponseEntity.ok(userService.register(user));
    }

    @PostMapping(path="/forgotPassword")
    public ResponseEntity<String> forgotPassword(@RequestBody User user){
        return ResponseEntity.ok(userService.forgotPassword(user));
    }

    @GetMapping(path="/findByToken/{token}")
    public ResponseEntity<User> findByToken(@PathVariable String token){
        return ResponseEntity.ok(userService.findByToken(token));
    }

    @PostMapping(path="/save")
    public ResponseEntity<User> save(@RequestBody User user){
        return ResponseEntity.ok(userService.save(user));
    }

    @GetMapping(path="/activate/{token}")
    public ResponseEntity<User> activate(@PathVariable String token){
        return ResponseEntity.ok(userService.activate(token));
    }

    @GetMapping(path="/findAll")
    public ResponseEntity<List<User>> findAll(){
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping(path="/delete/{id}")
    public ResponseEntity<Message> delete(@PathVariable Long id){
        return ResponseEntity.ok(userService.delete(id));
    }

    @GetMapping(path="/findByVendorId/{vendorId}")
    public ResponseEntity<List<User>> findByVendorId(@PathVariable long vendorId){
        return ResponseEntity.ok(userService.findByVendorId(vendorId));
    }

    @GetMapping(path="/findByDepartmentId/{departmentId}")
    public ResponseEntity<List<User>> findByDepartmentId(@PathVariable long departmentId){
        return ResponseEntity.ok(userService.findByDepartmentId(departmentId));
    }

}