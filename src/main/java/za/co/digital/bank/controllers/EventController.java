package za.co.digital.bank.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Event;
import za.co.digital.bank.services.EventService;

@RestController
@RequestMapping(path="/v1/event")
public class EventController extends AbstractController<EventService, Event> {
}