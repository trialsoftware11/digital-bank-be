package za.co.digital.bank.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Action;
import za.co.digital.bank.services.ActionService;

@RestController
@RequestMapping(path="/v1/action")
public class ActionController extends AbstractController<ActionService, Action> {
}