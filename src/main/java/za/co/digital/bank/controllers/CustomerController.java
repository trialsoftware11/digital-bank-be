package za.co.digital.bank.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Customer;
import za.co.digital.bank.services.CustomerService;

@RestController
@RequestMapping(path="/v1/customer")
public class CustomerController extends AbstractController<CustomerService, Customer> {
}