package za.co.digital.bank.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.digital.bank.model.Instruction;
import za.co.digital.bank.services.InstructionService;

@RestController
@RequestMapping(path="/v1/instruction")
public class InstructionController extends AbstractController<InstructionService, Instruction> {
}