package za.co.digital.bank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public abstract class AbstractDAO <T extends MongoRepository> {
    @Autowired
    T repository;

    public <S> S save(S entity){
        return (S) repository.save(entity);
    }

    public <S> S findById(Long id){
        return (S) repository.findById(id);
    }

    public <S> List<S> findAll(){
        return (List<S>) repository.findAll();
    }

    public <S> void delete(Long id){
        repository.deleteById(id);
    }
}