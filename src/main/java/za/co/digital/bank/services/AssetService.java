package za.co.digital.bank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.digital.bank.model.Asset;
import za.co.digital.bank.model.Customer;
import za.co.digital.bank.repositories.AssetRepository;
import za.co.digital.bank.repositories.CustomerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AssetService extends AbstractDAO<AssetRepository> {

    @Autowired
    CustomerRepository customerRepository;

    public List<Asset> findAll() {
        List<Asset> assets = repository.findAll();
        for (Asset asset : assets) {
            Optional<Customer> customer = customerRepository.findById(asset.getOwnerId());
            if (customer.isPresent()) {
                asset.setOwner(customer.get());
            }
        }
        return assets;
    }
}