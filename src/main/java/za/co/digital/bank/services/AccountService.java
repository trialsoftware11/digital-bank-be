package za.co.digital.bank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.digital.bank.model.Customer;
import za.co.digital.bank.repositories.AccountRepository;
import za.co.digital.bank.model.Account;
import za.co.digital.bank.repositories.CustomerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService extends AbstractDAO<AccountRepository>{

    @Autowired
    CustomerRepository customerRepository;

    public List<Account> findByOwnerId(Long ownerId){
        return repository.findByOwnerId(ownerId);
    }

    public List<Account> findAll(){
        List<Account> accounts = repository.findAll();
        for(Account account: accounts){
            Optional<Customer> owner = customerRepository.findById(account.getOwnerId());

            if(owner.isPresent()) {
                account.setOwner(owner.get());
            }
        }
        return accounts;
    }
}