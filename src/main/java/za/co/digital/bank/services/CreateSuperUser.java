package za.co.digital.bank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.digital.bank.enums.Status;
import za.co.digital.bank.model.UserRole;
import za.co.digital.bank.model.VendorUser;
import za.co.digital.bank.repositories.VendorUserRepository;

import javax.annotation.PostConstruct;

@Component
public class CreateSuperUser {

    @Autowired
    VendorUserRepository vendorUserRepository;

    @PostConstruct
    public void init(){
        System.out.println("Create Super User...");
        VendorUser superUser = new VendorUser();

       VendorUser found = vendorUserRepository.findByIdAndRole(1L, UserRole.SUPER_USER.name());

        if(found == null) {
            superUser.setRole(UserRole.SUPER_USER);
            superUser.setFirstName("Super");
            superUser.setLastName("User");
            superUser.setUsername("superadmin");
            superUser.setPassword("superadmin123");
            superUser.setStatus(Status.ACTIVE);

            vendorUserRepository.save(superUser);
        }
    }
}