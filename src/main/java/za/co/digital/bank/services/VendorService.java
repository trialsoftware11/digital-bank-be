package za.co.digital.bank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.digital.bank.repositories.VendorRepository;
import za.co.digital.bank.model.Vendor;
import za.co.digital.bank.repositories.DepartmentRepository;
import za.co.digital.bank.utils.Message;

import java.util.List;

@Service
public class VendorService {

    @Autowired
    VendorRepository vendorRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    SequenceGeneratorService sequenceGenerator;

    public Vendor save(Vendor vendor){
        if(vendor.getId() <= 0) {
            vendor.setId(sequenceGenerator.generateSequence(Vendor.SEQUENCE_NAME));
        }
        return vendorRepository.save(vendor);
    }

    public List<Vendor> findAll(){
        List<Vendor> vendors = vendorRepository.findAll();
        for(Vendor vendor: vendors){
            vendor.setDepartments(departmentRepository.findByVendorId(vendor.getId()));
        }
        return vendors;
    }

    public Vendor findById(long id) {
        return vendorRepository.findById(id).get();
    }

    public Message delete(Long id) {
        vendorRepository.deleteById(id);
        return new Message("Vendor deleted");
    }
}