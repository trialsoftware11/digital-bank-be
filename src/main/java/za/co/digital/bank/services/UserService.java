package za.co.digital.bank.services;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import za.co.digital.bank.enums.Status;
import za.co.digital.bank.model.*;
import za.co.digital.bank.repositories.ClientUserRepository;
import za.co.digital.bank.repositories.DepartmentUserRepository;
import za.co.digital.bank.repositories.VendorUserRepository;
import za.co.digital.bank.utils.Message;

import java.security.SecureRandom;
import java.util.*;

@Service
public class UserService {
    Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    DepartmentUserRepository departmentUserRepository;

    @Autowired
    VendorUserRepository vendorUserRepository;

    @Autowired
    ClientUserRepository clientUserRepository;

    @Autowired
    DozerBeanMapper dozerBeanMapper;

    @Autowired
    SequenceGeneratorService sequenceGenerator;

    @Autowired
    MailService mailService;

    @Value("${password.reset.url}")
    private String passwordResetUrl;

    @Value("${from.email}")
    private String fromEmail;

    @Value("${user.activation.url}")
    private String userActivationUrl;

    private static final SecureRandom secureRandom = new SecureRandom(); //threadsafe
    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe


    public User authenticate(User user) {
        LOGGER.info("Authenticating...");
        User found = departmentUserRepository.authenticate(user.getUsername(), user.getPassword());
        if (found == null) {
            found = vendorUserRepository.authenticate(user.getUsername(), user.getPassword());
        }

        if (found == null) {
            found = clientUserRepository.authenticate(user.getUsername(), user.getPassword());
        }

        //Check if user is active
        if(found != null && found.getStatus() != null && found.getStatus().equals(Status.ACTIVE)){
            return found;
        }

        return null;
    }

    public User register(User user) {
        LOGGER.info("Registering a new user...");

        ClientUser clientUser = new ClientUser();
        user.setActivationToken(generateToken());
        user.setRole(UserRole.CLIENT_USER);
        user.setStatus(Status.INACTIVE);
        dozerBeanMapper.map(user, clientUser);
        user = clientUserRepository.save(clientUser);

        //send activation email
        sendActivationEmail(user);

        return user;
    }

    private void sendActivationEmail(User user) {
        Mail mail = new Mail(fromEmail, user.getEmail(), "Account Activation");
        mail.getModel().put("name", user.getFirstName() + " " + user.getLastName());
        String activationUrl = userActivationUrl + "?tkn=" + user.getActivationToken();
        mail.getModel().put("href", activationUrl);
        mail.getModel().put("linkText", activationUrl);
        mailService.sendActivationEmail(mail);
    }

    public String forgotPassword(User user) {
        LOGGER.info("Forgot Password...");
        User found = departmentUserRepository.findByUsernameAndEmail(user.getUsername(), user.getEmail());
        if (found == null) {
            found = vendorUserRepository.findByUsernameAndEmail(user.getUsername(), user.getEmail());
        }

        if (found == null) {
            found = clientUserRepository.findByUsernameAndEmail(user.getUsername(), user.getEmail());
        }

        if (found != null) {
            Mail mail = new Mail(fromEmail, found.getEmail(), "Password Reset");
            mail.getModel().put("name", found.getFirstName() + " " + found.getLastName());

            String resetPasswordUrl = generateResetPasswordUrl(found);
            mail.getModel().put("href", resetPasswordUrl);
            mail.getModel().put("linkText", resetPasswordUrl);
            mailService.sendPasswordResetEmail(mail);
        }

        return "Password reset email sent to the user";
    }

    private String generateResetPasswordUrl(User found) {
        String token = generateToken();
        found.setForgotPasswordToken(token);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, 24);
        found.setForgotPasswordTokenExpiry(calendar.getTime());

        save(found);

        return passwordResetUrl + "?tkn=" + token;
    }

    private String generateToken() {
        byte[] randomBytes = new byte[64];
        secureRandom.nextBytes(randomBytes);
        String token = base64Encoder.encodeToString(randomBytes);
        return token;
    }

    public User save(User user) {
        if (user.getDepartmentId() > 0) {
            DepartmentUser newUser = new DepartmentUser();
            dozerBeanMapper.map(user, newUser);
            user = departmentUserRepository.save(newUser);
        } else if (user.getVendorId() > 0) {
            VendorUser newUser = new VendorUser();
            dozerBeanMapper.map(user, newUser);
            user = vendorUserRepository.save(newUser);
        } else {
            ClientUser newUser = new ClientUser();
            dozerBeanMapper.map(user, newUser);
            user = clientUserRepository.save(newUser);
        }
        return user;
    }

    public User findByToken(String token) {
        User found = vendorUserRepository.findByToken(token);

        if (found == null) {
            found = departmentUserRepository.findByToken(token);
        }

        if (found == null) {
            found = clientUserRepository.findByToken(token);
        }

        if (found != null && found.getForgotPasswordTokenExpiry().after(new Date())) {
            return found;
        }

        return null;
    }

    public User activate(String token) {
        User found = vendorUserRepository.findByActivationToken(token);

        if (found == null) {
            found = departmentUserRepository.findByActivationToken(token);
        }

        if (found == null) {
            found = clientUserRepository.findByActivationToken(token);
        }

        if (found != null) {
            found.setStatus(Status.ACTIVE);
            found.setActivationToken("");
            if (found.getVendorId() > 0) {
                VendorUser newUser = new VendorUser();
                dozerBeanMapper.map(found, newUser);
                found = vendorUserRepository.save(newUser);
            } else if (found.getDepartmentId() > 0) {
                DepartmentUser newUser = new DepartmentUser();
                dozerBeanMapper.map(found, newUser);
                found = departmentUserRepository.save(newUser);
            } else {
                ClientUser newUser = new ClientUser();
                dozerBeanMapper.map(found, newUser);
                found = clientUserRepository.save(newUser);
            }
            return found;
        }

        return null;
    }

    public List<User> findAll() {
        //Get All Vendor Users
        List<VendorUser> users = vendorUserRepository.findAll();
        List<User> converted = new ArrayList<>();

        users.stream().forEach((user) -> {
            converted.add(user);
        });

        //Get All Department Users
        List<DepartmentUser> departmentUsers = departmentUserRepository.findAll();
        departmentUsers.stream().forEach((user) -> {
            converted.add(user);
        });

        //Get All Client Users
        List<ClientUser> clientUsers = clientUserRepository.findAll();
        clientUsers.stream().forEach((user) -> {
            converted.add(user);
        });
        return converted;
    }

    public Message delete(Long id) {
        Optional<VendorUser> vendorUser = vendorUserRepository.findById(id);
        if (vendorUser.isPresent()) {
            vendorUserRepository.delete(vendorUser.get());
        } else {
            Optional<DepartmentUser> departmentUser = departmentUserRepository.findById(id);

            if (departmentUser.isPresent()) {
                departmentUserRepository.delete(departmentUser.get());
            } else {
                Optional<ClientUser> clientUser = clientUserRepository.findById(id);
                if (clientUser.isPresent()) {
                    clientUserRepository.delete(clientUser.get());
                }
            }
        }
        return new Message("User deleted");
    }


    public List<User> findByVendorId(long vendorId) {
        //Get All Vendor Users
        List<VendorUser> users = vendorUserRepository.findByVendorId(vendorId);
        List<User> converted = new ArrayList<>();

        users.stream().forEach((user) -> {
            converted.add(user);
        });


        //Get All Department Users that belong to this vendor
        List<DepartmentUser> departmentUsers = departmentUserRepository.findByVendorId(vendorId);
        departmentUsers.stream().forEach((user) -> {
            converted.add(user);
        });


        return converted;
    }

    public List<User> findByDepartmentId(long departmentId) {
        //Get All Department Users
        List<DepartmentUser> departmentUsers = departmentUserRepository.findByDepartmentId(departmentId);
        List<User> converted = new ArrayList<>();

        departmentUsers.stream().forEach((user) -> {
            converted.add(user);
        });

        return converted;
    }
}