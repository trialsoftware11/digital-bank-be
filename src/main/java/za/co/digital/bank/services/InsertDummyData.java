package za.co.digital.bank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.digital.bank.model.Dummy;
import za.co.digital.bank.repositories.DummyRepository;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
public class InsertDummyData {

    @Autowired
    DummyRepository dummyRepository;

    @PostConstruct
    public void init(){
        System.out.println("Inserting Dummy Data...");
        Dummy dummy = new Dummy();

        Optional<Dummy> found = dummyRepository.findById(dummy.get_id().toString());
        if(!found.isPresent()) {
            dummyRepository.insert(dummy);
        }
    }
}