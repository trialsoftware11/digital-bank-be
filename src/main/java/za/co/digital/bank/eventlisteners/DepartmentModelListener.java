package za.co.digital.bank.eventlisteners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;
import za.co.digital.bank.model.Department;
import za.co.digital.bank.services.SequenceGeneratorService;

@Component
public class DepartmentModelListener extends AbstractMongoEventListener<Department> {

    @Autowired
    SequenceGeneratorService sequenceGenerator;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Department> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId(sequenceGenerator.generateSequence(Department.SEQUENCE_NAME));
        }
    }
}