package za.co.digital.bank.eventlisteners;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;
import za.co.digital.bank.services.SequenceGeneratorService;

import java.lang.reflect.Field;
import java.util.List;

@Component
public class GenericModelListener <T> extends AbstractMongoEventListener<T> {

    @Autowired
    SequenceGeneratorService sequenceGenerator;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<T> event) {

        try {
            Field sequenceNameField = event.getSource().getClass().getDeclaredField("SEQUENCE_NAME");
            String sequenceName = (String) sequenceNameField.get(String.class);

            List<Field> idFields = FieldUtils.getFieldsListWithAnnotation(event.getSource().getClass(),Id.class);

            idFields.stream().forEach((field) -> {
                try {
                  Object fieldValue =  FieldUtils.readField(field,event.getSource(),true);

                  if(fieldValue instanceof Long){
                      if((long)fieldValue <= 0){
                        field.set(event.getSource(), sequenceGenerator.generateSequence(sequenceName));
                      }
                  }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}