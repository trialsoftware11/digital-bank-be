package za.co.digital.bank.eventlisteners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;
import za.co.digital.bank.services.SequenceGeneratorService;
import za.co.digital.bank.model.Vendor;

@Component
public class VendorModelListener extends AbstractMongoEventListener<Vendor> {

    @Autowired
    SequenceGeneratorService sequenceGenerator;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Vendor> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId(sequenceGenerator.generateSequence(Vendor.SEQUENCE_NAME));
        }
    }
}