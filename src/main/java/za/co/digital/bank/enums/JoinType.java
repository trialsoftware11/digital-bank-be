package za.co.digital.bank.enums;

public enum JoinType {
    AND,
    OR,
    NONE
}