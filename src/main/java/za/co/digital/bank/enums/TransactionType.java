package za.co.digital.bank.enums;

public enum TransactionType {
    STANDARD,
    DEPOSIT,
    WITHDDRAWAL,
    TRANSFER
}