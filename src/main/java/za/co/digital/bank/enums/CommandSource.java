package za.co.digital.bank.enums;

public enum CommandSource {
    PROCESS,
    STAFF,
    CUSTOMER,
    INTERNAL,
    SYSTEM,
    AUTOMATIC
}