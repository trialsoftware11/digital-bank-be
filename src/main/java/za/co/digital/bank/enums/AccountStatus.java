package za.co.digital.bank.enums;

public enum AccountStatus {
    ACTIVE,
    DISABLED,
    SUSPENDED
}