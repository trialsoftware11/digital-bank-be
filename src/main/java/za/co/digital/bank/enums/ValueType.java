package za.co.digital.bank.enums;

public enum ValueType {
    _CLASS,
    STRING_VALUE,
    DECIMAL_VALUE,
    INT_VALUE,
    DATE_VALUE
}