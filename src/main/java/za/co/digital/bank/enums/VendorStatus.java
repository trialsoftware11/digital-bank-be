package za.co.digital.bank.enums;

public enum VendorStatus {
    ACTIVE,
    SUSPENDED
}