package za.co.digital.bank.enums;

public enum EventType {
    SYSTEM,
    TRANSACTION,
    VENDOR,
    DEPARTMENT,
    CUSTOMER
}