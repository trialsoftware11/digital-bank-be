package za.co.digital.bank.enums;

public enum MarketStatus {
    SUSPENDED(0),
    NOT_AVAILABLE(1),
    ON_SALE(2);

    int value;

    MarketStatus(int value){
        this.value = value;
    }
}