package za.co.digital.bank.enums;

public enum AccountType {
    DEBIT,
    CREDIT,
    LOAN,
    INVESTMENT
}