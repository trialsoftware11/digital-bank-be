package za.co.digital.bank.enums;

public enum  ExecutionStatus {
    PENDING,
    ACTIVE,
    COMPLETED,
    ARCHIVED
}