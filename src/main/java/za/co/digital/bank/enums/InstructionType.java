package za.co.digital.bank.enums;

public enum InstructionType {
    PROCESS,
    VALIDATION,
    RULE,
    WORKFLOW
}