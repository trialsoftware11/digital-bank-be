package za.co.digital.bank.enums;

public enum ApprovalStatus {
    PENDING,
    APPROVED,
    REJECTED,
    SUSPENDED
}