package za.co.digital.bank.enums;

public enum AssetType {
    FINANCIAL,
    RESIDENCE,
    VEHICLE,
    PROPERTY,
    APPLIANCE,
    OTHER
}