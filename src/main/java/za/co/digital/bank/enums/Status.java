package za.co.digital.bank.enums;

public enum Status {
    ACTIVE(1),
    INACTIVE(0);

    private int value;

    Status(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}